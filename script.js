const respostaHtml = document.querySelector('#respostas');
const pergunta = document.querySelector('.inpt');
const respostas = [
  'Ceteza!',
  'Nao tenho certeza.',
  'E decididamente assim.',
  'Sem duvidas!',
  'Pergunte novamente mais tarde.',
  'Sim, definitivamente!',
  'Minha resposta e NAO!',
  'Melhor nao dizer nada agora.',
  'Voce pode contar com isso.',
  'Provavelmente',
  'Nao e possivel prever agora, desculpa.',
  'Lamentamos Mas nada a declar',
  'Processando suas informacoes',
  'Perscepectiva boa.',
  'As perspectivas nao sao tao boas assim',
  'Tente novamente',
  'Hummmm, acho que vai dar merda',
  'Hummm, melhor deixar isso de lado'
];



const btn = document.querySelector('button');
btn.addEventListener('click', () =>{

  if(pergunta.value == '') {
    alert('Preencha o campo!');
    return;
  }

  const pergun = '<div>' + pergunta.value + '</div>';

  const totalRespostas = respostas.length;
  const numeroAleatorio = Math.floor(Math.random() * totalRespostas);
  respostaHtml.innerHTML = pergun + respostas[numeroAleatorio];

  //sumir a resposta
  setTimeout(() => {
    respostaHtml.style.opacity = 0;
  }, 3000);
})
